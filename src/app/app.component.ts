import { Component, OnInit } from '@angular/core';

import { Factura } from './interfaces/factura.interface';
import { facturas as facturasBD } from './datos/facturas';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private facturas: Array<Factura> = facturasBD;
  public tituloApp: string = "Facturas";
  public balanceFake = 300;
  public total = {
    gasto: {},
    ingreso: {},
    balance: 0,
    tipoBalance: ""
  };
  public nuevaFactura: Factura = {
    id: 0,
    numero: "",
    base: 0,
    iva: 0,
    tipo: ""
  }

  public formVisible: boolean = false;

  public addFactura(): void {    
    this.facturas.push(Object.assign({}, this.nuevaFactura));
    this.formVisible = false;
  }

  public toggleForm(event): void {
    this.formVisible = !this.formVisible;
  }

  public calculaIva(base: number, iva: number): number {
    return base * iva / 100;
  }

  public calculaTotalFactura(factura: Factura): number {
    return factura.base + (factura.base * factura.iva / 100);
  }

  public nFacturasPorTipo(tipo: string): number {
    let facturasFiltradas: Array<Factura> = this.facturasPorTipo(tipo);
    return facturasFiltradas.length;
  }

  public facturasPorTipo(tipo: string): Array<Factura> {
    let facturasFiltradas: Array<Factura> = this.facturas.filter(factura => factura.tipo == tipo);
    return facturasFiltradas;
  }

  public totalFacturasPorTipo(tipo: string): number {
    let facturasTipo: Array<Factura> = this.facturasPorTipo(tipo);
    return facturasTipo.map((f: Factura): number => this.calculaTotalFactura(f))
      .reduce((t1: number, t2: number): number => t1 + t2);
  }

  public calcularTotalFacturas(): object {

    let objTotalesIngreso = {
      base: 0,
      iva: 0,
      total: 0
    };
    let objTotalesGasto = {
      base: 0,
      iva: 0,
      total: 0
    };

    this.facturas.forEach(f => {
      if (f.tipo === 'ingreso') {
        objTotalesIngreso.base += f.base;
        objTotalesIngreso.iva += this.calculaIva(f.base, f.iva);
        objTotalesIngreso.total += this.calculaTotalFactura(f);
      } else if (f.tipo === 'gasto') {
        objTotalesGasto.base += f.base;
        objTotalesGasto.iva += this.calculaIva(f.base, f.iva);
        objTotalesGasto.total += this.calculaTotalFactura(f);
      }
    });

    let balance: number = this.calcularBalance();

    this.total = {
      gasto: objTotalesGasto,
      ingreso: objTotalesIngreso,
      balance: balance,
      tipoBalance: -balance >= 0 ? "positivo" : "negativo"
    }

    return this.total;
  }

  public calcularBalance(): number {
    let balance: number = this.total.ingreso["total"] - this.total.gasto["total"];
    return balance;
  }

  ngOnInit(): void {

  }
}
