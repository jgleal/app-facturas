import { Factura } from '../interfaces/factura.interface';

export let facturas: Array<Factura>;

facturas = [
    {
        id: 1,
        numero: '1000',
        base: 100,
        iva: 21,
        tipo: 'ingreso'
    },
    {
        id: 2,
        numero: '1001',
        base: 50,
        iva: 21,
        tipo: 'ingreso'
    },
    {
        id: 3,
        numero: '1002',
        base: 300,
        iva: 21,
        tipo: 'ingreso'
    },
    {
        id: 4,
        numero: '1003',
        base: 75.5,
        iva: 21,
        tipo: 'ingreso'
    },
    {
        id: 5,
        numero: 'A003',
        base: 10,
        iva: 21,
        tipo: 'gasto'
    },                
    {
        id: 6,
        numero: '8003',
        base: 1.2,
        iva: 21,
        tipo: 'gasto'
    }
];