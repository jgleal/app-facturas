export interface Factura {
    id: number,
    numero: string,
    base: number,
    iva: number,
    tipo: string
}