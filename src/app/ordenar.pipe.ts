import { Pipe, PipeTransform } from '@angular/core';
import { Factura } from './interfaces/factura.interface';
@Pipe({
    name: 'ordenar'
})
export class OrdenarPipe implements PipeTransform {
    transform(arr: Array<Factura>): Array<Factura> {
        arr.sort((x, y) => {
            if (x.numero < y.numero) return -1;
            if (x.numero > y.numero) return 1;
            if (x.numero == y.numero) return 0;
        });
        return arr;
    }
}