import { Pipe, PipeTransform } from '@angular/core';

import { Factura } from '../interfaces/factura.interface';

@Pipe({
    name: "ordenarFactura"
})
export class OrdenarFacturaPipe implements PipeTransform {
    transform(facturas: Array<Factura>, ...args): Array<Factura> {
        let orden: string = args[0];
        let criterio: string = args[1];
        
        if (orden == undefined) orden = "asc";
        if (criterio == undefined) criterio = "numero";

        facturas.sort( (fact1: Factura, fact2: Factura) => {
            if (fact1[criterio] < fact2[criterio]) return orden == 'asc' ? -1 : 1;
            if (fact1[criterio] > fact2[criterio]) return orden == 'asc' ? 1 : -1;
            if (fact1[criterio] == fact2[criterio]) return 0;
        });
        
        return facturas;
    }
}